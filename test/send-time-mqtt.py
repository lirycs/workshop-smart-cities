"""Modual for sending time to the mqtt brocker"""
# Dieses Skript sendet auf das Topic "time" eine Uhrzeit. Alle 5 Minuten wird die Stunde um 1 erhöht.
# Es dient als Test für die ESP-Komponenten der Smart City.

import paho.mqtt.client as mqtt
import time
import datetime
import os
import json
from dotenv import load_dotenv

load_dotenv(dotenv_path="visualisierung_new/.env")

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT broker. Return Code: " + str(rc) + "\n---")
    else:
        raise Exception("Not able to connect to MQTT broker.")

try:
    client = mqtt.Client()
    client.on_connect = on_connect
    client.connect(os.getenv("MQTT_IP"), 1883)
    client.loop_start()
except Exception as e:
    raise e

zeit = datetime.datetime.utcnow()

while True:
    if client.is_connected():
        # Zeit an "zeit" senden
        topic = "zeit"
        hours, minutes, seconds = zeit.hour, zeit.minute, zeit.second
        msg = {
            "hour": hours,
            "minute": 0,
            "second": 0
        }
        client.publish(topic, json.dumps(msg))
        print("Topic: " + topic + "\nMessage: " + str(msg) + "\n---")

        # Zeit um eine Stunde hochzählen
        zeit = zeit + datetime.timedelta(hours=1)

        # warten
        time.sleep(10)