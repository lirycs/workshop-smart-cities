/*
 * Materialien:
 * 16in1_DE/Foto Widerstand Photoresistor Licht Sensor Modul LDR5528 für Arduino DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define DIGITAL_PIN 2 //GPIO2

boolean ldr = false;
String light;

void setup() {
  Serial.begin(9600); //Serial Monitor
  pinMode(DIGITAL_PIN, INPUT);
}

void loop() {
  ldr = digitalRead(DIGITAL_PIN);
  if (ldr) {
    light = "No";
  }
  else {
    light = "Yes";
  }
  Serial.print("Light detected: ");
  Serial.println(light);
  
  delay(2000);
}
