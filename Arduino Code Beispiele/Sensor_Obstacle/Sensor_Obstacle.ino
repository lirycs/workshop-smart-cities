/*
 * Materialien:
 * 16in1_DE/KY-032 Obstacle Detector Module DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 * Einstellen sehr umständlich und funktioniert nicht gut.
 */

#define SIGNAL_PIN 2

boolean object_detect = false;

void setup() {
  Serial.begin(9600);
  pinMode(SIGNAL_PIN, INPUT);
}

void loop() {
  if(digitalRead(SIGNAL_PIN)) {
    object_detect = false;
  }
  else {
    delayMicroseconds(500);
    if(digitalRead(SIGNAL_PIN)) {
      object_detect = false;
    }
    else {
      object_detect = true;
    }
  }
  if(object_detect == true) {
    Serial.println("Object Detected!");
    delay(100);
  }
  else {
    Serial.println("Nothing");
    delay(100);
  }
}
