/*
 * Materialien:
 * 16in1_DE/Regensensor_modul DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */


#define DIGITAL_PIN 4
#define ANALOG_PIN 0
#define SENSOR_POWER 2

uint16_t rainVal;
boolean isRaining = false;
String raining;

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
  pinMode(SENSOR_POWER, OUTPUT);
  digitalWrite(SENSOR_POWER, LOW);
}

void loop() {
  digitalWrite(SENSOR_POWER, HIGH);
  delay(10);
  rainVal = analogRead(ANALOG_PIN);
  isRaining = digitalRead(DIGITAL_PIN);
  digitalWrite(SENSOR_POWER, LOW);
  if (isRaining) {
    raining = "No";
  }
  else {
    raining = "Yes";
  }
  //rainVal = map(rainVal, 0, 1023, 100, 0);
  Serial.print("Raining: ");
  Serial.println(raining);
  Serial.print("Moisture: ");
  Serial.print(rainVal);
  Serial.println("%\n");
  delay(1000);
}
