/*
 * File >> Preferences >> Additional Board Manager URLs
 * https://dl.espressif.com/dl/package_esp32_index.json
 * Tools >> Board >> Boards Manager
 * "esp32" >> ESP32 by Expressif Systems >> Install
 * Tools >> Board >> ESP32 Arduino >> ESP32 Dev Module
 */

int loop_count = 1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Hello World!");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Hello Iteration!");
  for(int i = 0; i <= loop_count; i++) {
    Serial.println(i);
  }
  loop_count++;
  delay(1000);
}
