/*
 * Materialien:
 * 16in1_DE/KY-037 Big Microphone Modul DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Plotter
 * 
 * Einstellungen:
 * So lange an der Schraube drehen, bis die Baseline bei ca. 200 ist
 */


#define DIGITAL_PIN 2
#define ANALOG_PIN 26

void setup() {
  pinMode(DIGITAL_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  Serial.print("Digital: ");
  Serial.print(digitalRead(DIGITAL_PIN));
  Serial.print(" - Analog: ");
  Serial.println(analogRead(ANALOG_PIN));
  delay(10);
}
