/*
 * Materialien:
 * https://github.com/winlinvip/SimpleDHT
 * (16in1_DE/DHT11 Temperature Sensor Modul_DE.pdf)
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#include <SimpleDHT.h>
float temperature = 0;
float humidity = 0;
float humidity1 = 0;

int pinDHT11 = 4; //GPIO4
SimpleDHT11 dht11(pinDHT11);

//void setup() { 
  //Serial.begin(9600); //Serial Monitor
//}
float tempLoop() {
  
  Serial.println("Sample DHT11...");
  
  int err = SimpleDHTErrSuccess;
  
  if((err=dht11.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess){
    Serial.print("Read DHT11 failed, err=");
    Serial.println(err);
    
    delay(2000);
    
  }
  
  Serial.print("Sample OK: ");
  Serial.print((float)temperature);
  Serial.print(" *C, ");
  Serial.print((float)humidity);
  Serial.println(" RH%");

  delay(1500);
  return temperature;
}

float humidityLoop(){

  humidity = humidity1;

  return humidity1;

}

