/*
 * Materialien:
 * 16in1_DE/Regensensor_modul DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */


#define DIGITAL_PIN 15
#define ANALOG_PIN 6
#define SENSOR_POWER 0

uint16_t rainVal;
bool isRaining = false;
bool raining;

void rainSetup() {
  //Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
  pinMode(SENSOR_POWER, OUTPUT);
  digitalWrite(SENSOR_POWER, LOW);
}

bool rainLoop() {
  digitalWrite(SENSOR_POWER, HIGH);
  delay(10);
  rainVal = analogRead(ANALOG_PIN);
  isRaining = digitalRead(DIGITAL_PIN);
  digitalWrite(SENSOR_POWER, LOW);
  if (isRaining) {
    raining = false;
  }
  else {
    raining = true;
  }
  //rainVal = map(rainVal, 0, 1023, 100, 0);
  /*Serial.print("Raining: ");
  Serial.println(raining);
  Serial.print("Moisture: ");
  Serial.print(rainVal);
  */Serial.println("%\n");
  delay(1000);
  return raining;
  
}
