#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

char* Data;
char* Data1 = 0;

int zeit0 = 0;
int zeit1 = 0;
// Update these with values suitable for your network.
const char* ssid = "Leo_Laptop";
const char* password = "Leo_Laptop";
const char* mqtt_server = "87.106.170.226";
#define mqtt_port 1883
#define MQTT_USER "student"
#define MQTT_PASSWORD "Stud3nt"
#define MQTT_SERIAL_PUBLISH_TOPIC "katastrophenschutz/regen"
#define MQTT_SERIAL_PUBLISH_TOPIC1 "katastrophenschutz/temperature"
#define MQTT_SERIAL_RECEIVER_TOPIC "zeit"

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      client.publish("/test", "hello world");
      // ... and resubscribe
      client.subscribe(MQTT_SERIAL_RECEIVER_TOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte *payload, unsigned int length) {
  
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  if(!jsonDoc["hour"].isNull()) {
    zeit1 = jsonDoc["hour"].as<int>();
    Serial.println(zeit1);
  }

  if(!jsonDoc["int"].isNull()) {
    int number = jsonDoc["int"].as<int>();
    Serial.println(number);
  }

  JsonArray jsonArray = jsonDoc["array"];
  if (!jsonArray.isNull()) {
    for(JsonVariant v : jsonArray) {
      int number = v.as<int>();
      Serial.println(number);
    }
  }

  JsonObject jsonObj;
  jsonObj = jsonDoc.as<JsonObject>();
  jsonObj = jsonDoc["object"];

  /*
   * Sobald eine Nachricht gesendet wird, leert sich das JsonDoc automatisch. 
   * Deshalb erst alles empfangen und dann senden!
   */
}

void setup() {
  Serial.begin(9600);

  rainSetup();
  vibrationSetup();

  Serial.setTimeout(500);// Set time out
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();
}

void publishStringData(char *stringData){
  if (!client.connected()) {
    reconnect();
  }
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, stringData);
}

void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData){
  if (!client.connected()) {
    reconnect();
  }

  char buffer[measureJson(jsonData) + 1];
  serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, buffer, true);
}

void loop() {

  if(zeit1 != zeit0){
  Serial.print(rainLoop());
  
  //vibrationLoop();
  


  Serial.print("A");

  if(rainLoop() == true){
    Data = "Ja";

  } else{
    Data = "Nein";
  }

  Serial.print("B");





  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, Data);
  Serial.print("C");

  //client.publish(MQTT_SERIAL_PUBLISH_TOPIC1, Data1);
  Serial.print("D");
  
  
  zeit0 = zeit1;
  delay(100);  

  } 
  

   client.loop();
   if (!true) {
     publishStringData("{'test': 2}");
   }
 }
