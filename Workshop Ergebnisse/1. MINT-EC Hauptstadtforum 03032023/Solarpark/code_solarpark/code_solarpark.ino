#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>


// Update these with values suitable for your network.
const char* ssid = "AndroidAP266F";
const char* password = "Rolf1234";
const char* mqtt_server = "87.106.170.226";
#define mqtt_port 1883
#define MQTT_USER "student"
#define MQTT_PASSWORD "Stud3nt"
#define MQTT_SERIAL_PUBLISH_TOPIC "solarpark/energie"
#define MQTT_SERIAL_RECEIVER_TOPIC "[gebäude]/energie"
#define DIGITAL_PINdunkel 25 //GPIO25
#define DIGITAL_PINmittel 26 //GPIO26
#define DIGITAL_PINhell 27 //GPIO27

boolean ldrH = false;
boolean ldrM = false; 
boolean ldrD = false;
String light;

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      client.publish("/test", "hello world");
      // ... and resubscribe
      client.subscribe(MQTT_SERIAL_RECEIVER_TOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte *payload, unsigned int length) {
  
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  if(!jsonDoc["string"].isNull()) {
    String printable = jsonDoc["string"].as<String>();
    Serial.println(printable);
  }

  if(!jsonDoc["int"].isNull()) {
    int number = jsonDoc["int"].as<int>();
    Serial.println(number);
  }

  JsonArray jsonArray = jsonDoc["array"];
  if (!jsonArray.isNull()) {
    for(JsonVariant v : jsonArray) {
      int number = v.as<int>();
      Serial.println(number);
    }
  }

  JsonObject jsonObj;
  jsonObj = jsonDoc.as<JsonObject>();
  jsonObj = jsonDoc["object"];

  /*
   * Sobald eine Nachricht gesendet wird, leert sich das JsonDoc automatisch. 
   * Deshalb erst alles empfangen und dann senden!
   */
}

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PINhell, INPUT);
  pinMode(DIGITAL_PINmittel, INPUT);
  pinMode(DIGITAL_PINdunkel, INPUT);
  Serial.setTimeout(500);// Set time out
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();
}

void publishStringData(char *stringData){
  if (!client.connected()) {
    reconnect();
  }
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, stringData);
}

void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData){
  if (!client.connected()) {
    reconnect();
  }

  char buffer[measureJson(jsonData) + 1];
  serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, buffer, true);
}

void loop() {
  ldrH = digitalRead(DIGITAL_PINhell);
  ldrM = digitalRead(DIGITAL_PINmittel);
  ldrD = digitalRead(DIGITAL_PINdunkel);
  if (!ldrH) {
    light = "Yes Hell";
  }
  else if (!ldrM) {
    light = "Yes Mittel";
  }
  else if (!ldrD) {
    light = "Yes Dunkel";
  }
  else 
  {
    light = "No";
  }
  
  Serial.print("Light detected: ");
  Serial.println(light);
  client.loop();
   if (light == "Yes Hell") {
     publishStringData("{'available': 1200}");
   }
   else if (light == "Yes Mittel") {
     publishStringData("{'available': 1000}");
   }
   else if (light == "Yes Dunkel") {
     publishStringData("{'available': 800}");
   }
   else {
     publishStringData("{'available': 700}");
   }
   
   delay (5000);
   }
 
