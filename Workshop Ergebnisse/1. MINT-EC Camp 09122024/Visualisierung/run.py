from app import app, socketio
from app.models.mqtt_handeler import MQTTClient
from app.models.logger import Logger
from dotenv import load_dotenv
import os

load_dotenv()

mqttc = MQTTClient(os.getenv("MQTT_IP"), int(os.getenv("MQTT_PORT")))


if __name__ == "__main__":
    try:
        # try running Flask app with Websockets
        if mqttc.connect():
            mqttc.subscribe(
                [
                    "zeit",
                    "flughafen/flugverbot",
                    "flughafen/flugzeug",
                    "wetterstation/prognose",
                    "wetterstation/barometer",
                    "solarpark/energie/feuerwache",
                    "solarpark/energie/hochhaus",
                    "solarpark/energie/tiefgarage",
                    "solarpark/energie/einkaufszentrum",
                    "solarpark/energie/katastrophenschutz",
                    "solarpark/energie/wetterstation",
                    "solarpark/energie/solarpark",
                    "solarpark/energie/flughafen",
                    "katastrophenschutz/alarm",
                    "einkaufszentrum/offen",
                ]
            )

        Logger.setup_flask_logger(app=app, log_file="./logs/app.log")
        app.logger.info("Starting the app....")
        socketio.run(app, host="0.0.0.0", port=5000, debug=True)

    except Exception as e:
        app.logger.error("Some unknown error occured.", str(e))
        raise SystemExit
    mqttc.disconnect()
