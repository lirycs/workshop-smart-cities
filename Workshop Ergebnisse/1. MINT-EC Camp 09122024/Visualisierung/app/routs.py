"""Modual for managing the flask routs"""
from app import app, socketio
from flask import render_template

@app.route('/')
def home():
    """Route to the homepage"""
    return render_template('home.html')

@app.route('/dashboard')
def dashboard():
    """Route to the dashboard"""
    return render_template('dashboard.html')

@app.route('/mall')
def mall():
    """Route to the mall"""
    return render_template('mall.html')

@app.route('/logs')
def logs():
    """Route to the logs"""
    return render_template('logs.html', logs=[])

@socketio.on('connect')
def handle_connect():
    """Handling connection to the Web-Browser"""
    app.logger.info('Connected to Web-Browser')