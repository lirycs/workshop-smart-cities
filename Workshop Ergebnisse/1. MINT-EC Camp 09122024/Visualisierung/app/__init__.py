from flask import Flask
from flask_socketio import SocketIO

app = Flask("MQTT-APP", template_folder="./app/templates", static_folder="./app/static")
socketio = SocketIO(app)

from app import routs
from app.logic import logic
from app.models import mqtt_handeler
