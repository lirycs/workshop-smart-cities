"""Modual for handeling logic forthe mqtt broker"""

import paho.mqtt.client as mqtt
from typing import List
import json
from app.models.logger import Logger
from app import socketio
from datetime import date, timedelta


class MQTTClient:
    """Class for connecting to a mqtt-broker/host

    Parameters:
    broker (str): The adress of the broker
    port (str)[optional]: The port of the broker

    Example usage:
        mqtt_client = MQTTClient()
        mqtt_client.connect()
        mqtt_client.subscribe("test/topic")

        # Get messages manually
        message = mqtt_client.get_message()

        # Or use callback
        mqtt_client.register_callback(lambda msg: print(f"New message: {msg}"))"""

    def __init__(self, broker: str = "localhost", port: int = 1883):
        self.broker = broker
        self.port = port
        self.client = mqtt.Client()
        self.energylevels = {}

        # Configure logging
        self.logger = Logger.setup_mqtt_logger(
            log_file="visualisierung_new/logs/mqtt.log"
        )

        # Set callbacks
        self.client.on_connect = self._on_connect
        self.client.on_message = self._on_message

    def _on_connect(self, client, userdata, flags, rc):
        self.logger.info(f"Connected to {self.broker} with result code {rc}")

    def _on_message(self, client, userdata, msg):
        try:
            msgjson = json.loads(msg.payload.decode())
            self.logger.info(f"Received message on {msg.topic}: {msgjson}")
            socketio.emit("mqtt_message", msgjson)

            if msg.topic == "zeit":
                socketio.emit("msg_time", msgjson["hour"])
                d = date(2025, 1, 1)
                dd = timedelta(days=msgjson["day"])
                d = d + dd
                dstr = d.strftime("%d.%m.%Y")
                socketio.emit("msg_date", dstr)

            if msg.topic == "flughafen/flugzeug":
                socketio.emit("msg_plane", f"width: {msgjson['y']/2}%")

            if msg.topic == "flughafen/flugverbot":
                socketio.emit("msg_flightban", msgjson["flugverbot"])

            if msg.topic == "einkaufszentrum/offen":
                socketio.emit("msg_shops", msgjson["state"])

            if msg.topic == "wetterstation/prognose":
                txt = f"{msgjson['nässe']['regenwahrscheinlichkeit']}\n{msgjson['nässe']['taupunkt']}\n{msgjson['veränderung']['veränderung des luftdrucks']}\n{msgjson['veränderung']['veränderung der temperatur']}"
                socketio.emit("msg_prognose", txt)

            if msg.topic == "wetterstation/barometer":
                txt = f"{msgjson['temperature']['value']}°C\n{msgjson['presure']['value']-1E5}Pa"
                socketio.emit("msg_barometer", txt)

            if msg.topic.startswith("solarpark/energie"):
                key = msg.topic.split("/")[-1].capitalize()
                if key == "gesamt":
                    return

                self.energylevels[key] = msgjson["value"]

                data = {
                    "labels": list(self.energylevels.keys()),
                    "datasets": [
                        {
                            "label": "kW",
                            "data": list(self.energylevels.values()),
                            "borderWidth": 1,
                            # "backgroundColor": [
                            #     "#CB4335",
                            #     "#1F618D",
                            #     "#F1C40F",
                            #     "#27AE60",
                            #     "#884EA0",
                            #     "#D35400",
                            # ],
                        }
                    ],
                }
                socketio.emit("msg_energy", data)

            if msg.topic == "katastrophenschutz/alarm":
                if msgjson["severity"] > 0:
                    socketio.emit(
                        "msg_alert",
                        f"ALARM: {msgjson['type']}<br>STUFE: {msgjson['severity']}",
                    )
                else:
                    socketio.emit("msg_alert", "")

            if msg.topic == "$SYS/broker/clients/total":
                socketio.emit(
                    "clients", {"data": msg.payload.decode(), "topic": msg.topic}
                )

        except json.JSONDecodeError:
            self.logger.error("Failed to decode message as JSON")

    def connect(self) -> bool:
        try:
            self.client.connect(self.broker, self.port, 60)
            self.client.loop_start()
            return True
        except Exception as e:
            self.logger.error(f"Connection failed: {e}")
            return False

    def disconnect(self):
        self.client.loop_stop()
        self.client.disconnect()

    def subscribe(self, topics: List):
        for topic in topics:
            self.client.subscribe(topic)
            self.logger.info(f"Subscribed to {topic}")

    def publish(self, topic: str, message: dict):
        try:
            self.client.publish(topic, json.dumps(message))
            self.logger.info(f"Published to {topic}: {message}")
        except Exception as e:
            self.logger.error(f"Failed to publish message: {e}")
