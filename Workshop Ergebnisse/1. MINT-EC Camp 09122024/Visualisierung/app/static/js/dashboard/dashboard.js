const socket = io();
let messageCount = 0;
const maxMessages = 100;

// JavaScript - Hilfe https://www.w3schools.com/js/default.asp

// Chart.js - Hilfe https://www.chartjs.org/docs/latest/getting-started/


socket.on('connect', () => {
    document.getElementById('connectionStatus').textContent = 'Connected';
});

socket.on('disconnect', () => {
    document.getElementById('connectionStatus').textContent = 'Disconnected';
});

socket.on('clients', (message) => {
    document.getElementById('messageClient').textContent = JSON.stringify(message, null, 2)
});

socket.on('mqtt_message', (message) => {
    // Update message count
    messageCount++;
    document.getElementById('messageCount').textContent = messageCount;

    // Update last message
    document.getElementById('messageContent').textContent = 
        JSON.stringify(message, null, 2);

    // Add to message history
    const messageElement = document.createElement('div');
    messageElement.className = 'p-2 bg-gray-50 rounded';
    messageElement.textContent = `${new Date().toLocaleTimeString()}: ${JSON.stringify(message)}`;
    
    const history = document.getElementById('messageHistory');
    history.insertBefore(messageElement, history.firstChild);

    if (history.children.length > maxMessages) {
        history.removeChild(history.lastChild);
    }
});
