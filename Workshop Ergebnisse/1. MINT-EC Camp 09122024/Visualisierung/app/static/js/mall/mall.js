const socket = io();
let messageCount = 0;
const maxMessages = 100;
let lastHour = Date.now();
let hour = 0;
let minute = 0;
const hourTime = 10000;

// JavaScript - Hilfe https://www.w3schools.com/js/default.asp

// Chart.js - Hilfe https://www.chartjs.org/docs/latest/getting-started/

setInterval(interpolateTime, 80)


const ctx = document.getElementById('EnergyChart');
const energyChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: [],
        datasets: [{
            label: 'kW',
            data: [],
        }]
    },
    options: {
        animation: false,
        animations: {
            duration: 0
        },
    }
});

function interpolateTime() {
    minute = (Date.now() - lastHour) * 60 / 10000
    document.getElementById('time').textContent = hour.toString().padStart(2, "0") + ":" + Math.floor(minute).toString().padStart(2, "0")
}


socket.on('connect', () => {
    document.getElementById('connectionStatus').textContent = 'Connected';
});

socket.on('disconnect', () => {
    document.getElementById('connectionStatus').textContent = 'Disconnected';
});

socket.on('clients', (message) => {
    document.getElementById('messageClient').textContent = JSON.stringify(message, null, 2)
});

// socket.on('mqtt_message', (message) => {
//     // Update message count
//     messageCount++;
//     document.getElementById('messageCount').textContent = messageCount;

//     // Update last message
//     document.getElementById('messageContent').textContent =
//         JSON.stringify(message, null, 2);

//     // Add to message history
//     const messageElement = document.createElement('div');
//     messageElement.className = 'p-2 bg-gray-50 rounded';
//     messageElement.textContent = `${new Date().toLocaleTimeString()}: ${JSON.stringify(message)}`;

//     const history = document.getElementById('messageHistory');
//     history.insertBefore(messageElement, history.firstChild);

//     if (history.children.length > maxMessages) {
//         history.removeChild(history.lastChild);
//     }
// });

socket.on('msg_time', (message) => {
    hour = message;
    lastHour = Date.now()
});

socket.on('msg_date', (message) => {
    document.getElementById('date').textContent = message
});

socket.on('msg_shops', (message) => {
    document.getElementById('shop').innerHTML = ((message == "open") ? '<h3 class="text-success">Einkaufszentrum hat geöffnet</h3>' : '<h3 class="text-danger">Einkaufszentrum hat geschlossen</h3>')
});

socket.on('msg_alert', (message) => {
    document.getElementById('alert').innerHTML = message
});


socket.on('msg_prognose', (message) => {
    document.getElementById('prognose').textContent = message
});

socket.on('msg_barometer', (message) => {
    document.getElementById('barometer').textContent = message;
});

socket.on("msg_plane", (message) => {
    document.getElementById('planeheight').style = message;
    console.log(message);
});

socket.on("msg_flightban", (message) => {
    let bar = document.getElementById('planeheight')
    if (message) {
        bar.classList.add("bg-danger")
        bar.classList.remove("bg-success")
    } else {
        bar.classList.add("bg-success")
        bar.classList.remove("bg-danger")
    }
    document.getElementById('fText').innerHTML = message ? '<h4 class="text-center text-danger">Flugverbot</h4>' : '<h4 class="text-center text-success">Kein Flugverbot</h4>';
});

socket.on("msg_energy", (message) => {
    console.log(message);

    energyChart.data = message;

    energyChart.update();
});