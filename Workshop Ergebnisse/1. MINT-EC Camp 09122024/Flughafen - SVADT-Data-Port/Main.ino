/*
 * Materialien:
 * 16in1_DE/Regensensor_modul DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */


#define RAIN_PIN 35
#define HUMIDITY_PIN 32
#include <Adafruit_BMP085.h>
#define TRIG_PIN2 27 //GPIO4
#define ECHO_PIN2 14 //GPIO2
#define TRIG_PIN1 15
#define ECHO_PIN1 2
#define TRIG_PIN3 0
#define ECHO_PIN3 4
#define RED 19
#define YELLOW 18
#define GREEN 5
#define FIREPIN 36
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
// Update these with values suitable for your network.
#define mqtt_port 1883
#define MQTT_USER "SVADT DataPort"
//#define MQTT_PASSWORD "MQTT_PASS"
#define COMET_BLINKS 16
#define BLINKS 8



Adafruit_BMP085 bmp;

TaskHandle_t TaskHandle;

long duration, cm;
bool flugverbotNightRest = false;
bool flugverbotAlarm = false;
bool flugverbotWeather = false;
bool flugverbotSound = false; 
bool flugverbotFire = false;
int terminals;
int cometAlertPhase;

int cometAlertBlink, blink;

long planeHeight = 200;

const int z2 = 27, x3 = 30, z3 = 11;
const int z2s = z2*z2, x3s = x3*x3, z3s = z3*z3;
const int r = 4;

// Update these with values suitable for your network.
const char* ssid = "MQTT-MINT";
//const char* password = "AP_PASS";
const char* mqtt_server = "10.42.0.1";

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), MQTT_USER, "")) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      client.publish("/test", "hello world");
      // ... and resubscribe
      client.subscribe("zeit");
      client.subscribe("katastrophenschutz/alarm");
      client.subscribe("flughafen/beschwerde");
      client.subscribe("solarpark/energie/flughafen");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

int readHumidity(){
  return 100 - 100*analogRead(HUMIDITY_PIN)/4096;
}

bool readRain(){
  return !digitalRead(RAIN_PIN);
}

long readDistance1(){
  digitalWrite(TRIG_PIN1, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG_PIN1, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN1, LOW);
  
  duration = pulseIn(ECHO_PIN1, HIGH);
  cm = (duration / 2) / 29.1;
  return cm;
}

long readDistance2(){
  digitalWrite(TRIG_PIN2, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG_PIN2, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN2, LOW);
  
  duration = pulseIn(ECHO_PIN2, HIGH);
  cm = (duration / 2) / 29.1;
  return cm;
}

long readDistance3(){
  digitalWrite(TRIG_PIN3, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG_PIN3, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN3, LOW);
  
  duration = pulseIn(ECHO_PIN3, HIGH);
  cm = (duration / 2) / 29.1;
  return cm;
}

bool triangulate(long *x, long *y, long *z){
  long d3 = readDistance3() + r;
  long d2 = readDistance2() + r;
  long d1 = readDistance1() + r;
  
  long d1s = d1*d1;
  long d2s = d2*d2;
  long d3s = d3*d3;
  
  *z = (z2s + d1s - d2s)/(2*z2);
  *x = (x3s - 2*z3*(*z)-z3s+d1s-d3s)/(2*x3);
  *y = long(sqrt(d1s - (*x)*(*x) - (*z)*(*z)));

  return (*y) != 2147483647;
}

void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData, const char* topic){
  if (!client.connected()) {
    reconnect();
  }

  char buffer[measureJson(jsonData) + 1];
  size_t n = serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(topic, buffer, n);
}

void publishSensorData(StaticJsonDocument<BUFFER_SIZE> timeObj){
  bool raining = readRain();
  int humidity = readHumidity();
  float temperature = bmp.readTemperature();
  float pressure = bmp.readPressure();

  StaticJsonDocument<BUFFER_SIZE> barometer;

  JsonObject tempObj = barometer.createNestedObject("temperature");
  tempObj["value"] = temperature;
  tempObj["unit"] = "°C";

  JsonObject pressObj = barometer.createNestedObject("pressure");
  pressObj["value"] = pressure;
  pressObj["unit"] = "Pa";
  
  publishJsonData(barometer, "flughafen/barometer");


  StaticJsonDocument<BUFFER_SIZE> rain;

  JsonObject rainObj = rain.createNestedObject("rain");
  rainObj["value"] = raining;

  JsonObject moistObj = rain.createNestedObject("moisture");
  moistObj["value"] = humidity;
  moistObj["unit"] = "%";
  
  publishJsonData(rain, "flughafen/regen");

  StaticJsonDocument<BUFFER_SIZE> weather;

  JsonObject tempObjW = weather.createNestedObject("temperature");
  tempObjW["value"] = temperature;
  tempObjW["unit"] = "°C";

  JsonObject pressObjW = weather.createNestedObject("pressure");
  pressObjW["value"] = pressure;
  pressObjW["unit"] = "Pa";

  JsonObject rainObjW = weather.createNestedObject("rain");
  rainObjW["value"] = raining;

  JsonObject moistObjW = weather.createNestedObject("moisture");
  moistObjW["value"] = humidity;
  moistObjW["unit"] = "%";

  publishJsonData(weather, "flughafen/wetter");

  StaticJsonDocument<BUFFER_SIZE> rainReport;
  
  rainReport["sensor"] = "rain";
  rainReport["origin"] = "flughafen";

  JsonObject rainData = rainReport.createNestedObject("data");
  JsonObject rainDataRain = rainData.createNestedObject("rain");
  rainDataRain["value"] = raining;
  
  JsonObject rainDataMoisture = rainData.createNestedObject("moisture");
  rainDataMoisture["value"] = humidity;
  rainDataMoisture["unit"] = "%";

  rainData["time"] = timeObj;

  publishJsonData(rainReport, "katastrophenschutz/report");

  delay(100);

  StaticJsonDocument<BUFFER_SIZE> baroReport;
  
  baroReport["sensor"] = "barometer";
  baroReport["origin"] = "flughafen";

  JsonObject baroData = baroReport.createNestedObject("data");
  JsonObject baroDataTemperature = baroData.createNestedObject("temperature");
  baroDataTemperature["value"] = temperature;
  baroDataTemperature["unit"] = "°C";
  
  JsonObject baroDataPressure = baroData.createNestedObject("pressure");
  baroDataPressure["value"] = pressure;
  baroDataPressure["unit"] = "Pa";

  rainData["time"] = timeObj;

  publishJsonData(baroReport, "katastrophenschutz/report");
}


void publishFlugverbot(){
  bool flugverbot = flugverbotNightRest ||flugverbotAlarm || flugverbotWeather || flugverbotSound || flugverbotFire;
  String reason = "-";
  if (flugverbotFire){
    reason = "fire";
  } else if (flugverbotAlarm){
    reason = "alarm";
  } else if (flugverbotNightRest){
    reason = "night rest";
  } else if (flugverbotWeather){
    reason = "weather";
  } else if (flugverbotSound){
    reason = "sound";
  }
  
  StaticJsonDocument<BUFFER_SIZE> flugverbotJson;

  flugverbotJson["flugverbot"] = flugverbot;
  flugverbotJson["reason"] = reason;
  
  publishJsonData(flugverbotJson, "flughafen/flugverbot");

  publishTerminals();
}

void publishTerminals(){
  StaticJsonDocument<BUFFER_SIZE> terminalsJson;

  terminalsJson["terminals"] = terminals;
  
  publishJsonData(terminalsJson, "flughafen/terminals");


  StaticJsonDocument<BUFFER_SIZE> energyJson;

  energyJson["value"] = terminals*50 + 100;
  energyJson["unit"] = "kW";
  
  publishJsonData(energyJson, "flughafen/energie/verbrauch");
}


void callback(char* topic, byte *payload, unsigned int length) {
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);
  
  String sTopic = topic;
  if (sTopic.equals("zeit")){
    flugverbotSound = false;
    
    if(!jsonDoc["hour"].isNull()) {
      int hour = jsonDoc["hour"].as<int>();
      flugverbotNightRest = hour >= 22 || hour < 6;
    } 

    publishFlugverbot();
    
    publishSensorData(jsonDoc);
  } else if(sTopic.equals("flughafen/beschwerde")){
    flugverbotSound = true;
    publishFlugverbot();
  } else if(sTopic.equals("katastrophenschutz/alarm")){
    flugverbotAlarm = !jsonDoc["alarm"].isNull() && !jsonDoc["alarm"]["actions"].isNull() && !jsonDoc["alarm"]["actions"]["flughafenschließung"].isNull() && !jsonDoc["alarm"]["actions"]["flughafenschließung"].as<bool>();
    publishFlugverbot();
  } else if(sTopic.equals("solarpark/energie/flughafen")){
    if(!jsonDoc["value"].isNull()) {
      int supplyPower = jsonDoc["value"].as<int>();
      terminals = 1 + (supplyPower >= 200 ? 1 : 0) + (supplyPower >= 250 ? 1 : 0);
      if (flugverbotNightRest ||flugverbotAlarm || flugverbotWeather || flugverbotSound || flugverbotFire){
        terminals = 0;
      }
      publishTerminals();
    }
  }
}


void setup() {
  Serial.begin(9600);
  Serial.setTimeout(500);// Set time out

  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();

  pinMode(RAIN_PIN, INPUT);
  pinMode(FIREPIN, INPUT);

  pinMode(TRIG_PIN1, OUTPUT);
  pinMode(ECHO_PIN1, INPUT);
  pinMode(TRIG_PIN2, OUTPUT);
  pinMode(ECHO_PIN2, INPUT);
  pinMode(TRIG_PIN3, OUTPUT);
  pinMode(ECHO_PIN3, INPUT);

  pinMode(RED, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(GREEN, OUTPUT);
  
  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }
}

void cometAlert(){
  StaticJsonDocument<BUFFER_SIZE> cometReport;

  cometReport["type"] = "comet";
  cometReport["origin"] = "flughafen";
  
  publishJsonData(cometReport, "katastrophenschutz/meldungen");
  
  cometAlertBlink = COMET_BLINKS + (cometAlertBlink&1);
}

void meassurePlane(){
  long x, y, z;
  bool valid = triangulate(&x, &y, &z);
  
  if (valid){
    Serial.print(x);
    Serial.print(" | ");
    Serial.print(y);
    Serial.print(" | ");
    Serial.println(z);
    Serial.println();

    if (planeHeight - y > 5){
      cometAlertPhase++;
      if (cometAlertPhase == 3){
        cometAlert();
      }
    } else {
      cometAlertPhase = 0;
    }

    planeHeight = y;

    StaticJsonDocument<BUFFER_SIZE> flugzeug;

    flugzeug["x"] = x;
    flugzeug["y"] = y;
    flugzeug["z"] = z;
  
    publishJsonData(flugzeug, "flughafen/flugzeug");
  }
}

void updateAmpel(){
  if (flugverbotNightRest ||flugverbotAlarm || flugverbotWeather || flugverbotSound || flugverbotFire) {
    digitalWrite(RED, HIGH);
    digitalWrite(YELLOW, LOW);
    digitalWrite(GREEN, LOW);
    if (planeHeight < 100){
      blink = BLINKS + (blink&1);
    }
  } else if (planeHeight < 100) {
    digitalWrite(RED, LOW);
    digitalWrite(YELLOW, HIGH);
    digitalWrite(GREEN, LOW);
  } else {
    digitalWrite(RED, LOW);
    digitalWrite(YELLOW, LOW);
    digitalWrite(GREEN, HIGH);
  }
}


void publishFireAlarm() {
  StaticJsonDocument<BUFFER_SIZE> fireAlarmJson;

  fireAlarmJson["alarm"] = true;
  fireAlarmJson["origin"] = "flughafen";
  fireAlarmJson["type"] = "fire";
  
  
  publishJsonData(fireAlarmJson, "feuerwache/alarm");
  flugverbotFire = true;
  publishFlugverbot();
}

void loop() {
  client.loop();

  float T = bmp.readTemperature();
  if (flugverbotWeather != (T <= 4)){
     flugverbotWeather = true;
     publishFlugverbot();
  }

  if (flugverbotFire != (digitalRead(FIREPIN) == LOW)){
    flugverbotFire = digitalRead(FIREPIN) == LOW;
    if (flugverbotFire) publishFireAlarm();
  }
  
  meassurePlane();

  updateAmpel();
  if (cometAlertBlink > 0 || blink > 0){
    if (cometAlertBlink > 0){
      cometAlertBlink--;
    }
    if (blink > 0){
      blink--;
    }
    int bState = (cometAlertBlink <= 0 ?(( blink & 1 ) == 1 ): (( cometAlertBlink & 1 ) == 1 )) ? HIGH : LOW;
    digitalWrite(RED, bState);
    
    if (cometAlertBlink > 0){
      digitalWrite(YELLOW, bState);
      digitalWrite(GREEN, bState);
    }
  }
  
  delay(100);
}
