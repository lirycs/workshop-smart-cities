//Katastrophenschutz
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SimpleDHT.h>

// DHT11 Sensor temp und feuchte
int pinDHT11 = 15; // GPIO2
SimpleDHT11 dht11(pinDHT11);

// Regen- und Vibrationssensor
#define DIGITAL_PIN_RAIN 4
#define ANALOG_PIN_RAIN 0
#define DIGITAL_PIN_VIBRATION 2
bool isRaining = false;

//Vibration
bool vibrating = false;

//extern Hochwasser
int tmoisture;

//extern Komet
String tType;

// MQTT
const char* ssid = "MQTT-MINT";
const char* mqtt_server = "10.42.0.1";
#define mqtt_port 1883
#define MQTT_SERIAL_PUBLISH_TOPIC "PUBLISH_TOPIC"
#define MQTT_SERIAL_RECEIVER_TOPIC "RECEIVE_TOPIC"

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);
WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish("/test", "hello world");
      client.subscribe("zeit");
      client.subscribe("feuerwache/alarm");
      client.subscribe("katastrophenschutz/meldung");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}
void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData, const char *topic) {
  if (!client.connected()) {
    reconnect();
  }
  size_t len = measureJson(jsonData) + 1;
  char buffer[len];
  size_t n = serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(topic, buffer, n);
}

void callback(char* topic, byte *payload, unsigned int length) {
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  //Alarm Überschwemmung
  String topic_to_compare3 = "wetterstation/regen";
  if (topic_to_compare3.equals(topic))
  {
    if (!jsonDoc["moisture"].isNull()) {
      tmoisture = jsonDoc["moisture"].as<int>();
      Serial.println(tmoisture);

    }


  }
  //Alarm Komet
  String topic_to_compare2 = "katastrophenschutz/meldung";
  if (topic_to_compare2.equals(topic))
  {

    if (!jsonDoc["type"].isNull()) {
      tType = jsonDoc["type"].as<String>();
      Serial.println(tType);

    }
  }


  String topic_to_compare = "zeit";
  if (topic_to_compare.equals(topic)) {
    float temperature = 0;
    float humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read2(&temperature, &humidity, NULL)) == SimpleDHTErrSuccess) {
      StaticJsonDocument<BUFFER_SIZE> temp;
      temp["temp in °C"] = (float)temperature;
      temp["humidity in RH%"] = (float)humidity;
      publishJsonData(temp, "katastrophenschutz/temperatur");

      //Verbrauch
      //Leeres JSON Dokument erstellen
      StaticJsonDocument<BUFFER_SIZE> energieVerbrauch;
      energieVerbrauch["value"] = 50;
      energieVerbrauch["unit"] = "kw";
      publishJsonData(energieVerbrauch, "katastrophenschutz/energie/verbrauch");

      //Vibration
      StaticJsonDocument<BUFFER_SIZE> vibration;
      vibration["vibration"] = vibrating;
      publishJsonData(vibration, "katastrophenschutz/vibration");

      //Regen
      StaticJsonDocument<BUFFER_SIZE> rain;
      rain["isRaining"] = isRaining;
      publishJsonData(rain, "katastrophenschutz/rain");

      //report
      StaticJsonDocument<BUFFER_SIZE> report;
      report["isRaining"] = isRaining;
      report["vibration"] = vibrating;
      report["temp in °C"] = (float)temperature;
      report["humidity in RH%"] = (float)humidity;
      publishJsonData(report, "katastrophenschutz/report");
    }
  }
}




void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();

  pinMode(DIGITAL_PIN_RAIN, INPUT);
  pinMode(DIGITAL_PIN_VIBRATION, INPUT);
}



void loop() {
  client.loop();

  // DHT11 Sensor
  Serial.println("Sample DHT11...");
  float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;

  if ((err = dht11.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err=");
    Serial.println(err);
    delay(200);
  } else {
    Serial.print("Sample OK: ");
    Serial.print((float)temperature);
    Serial.print(" °C, ");
    Serial.print((float)humidity);
    Serial.println(" RH%");
  }

  // Regen Sensor
  uint16_t rainVal = analogRead(ANALOG_PIN_RAIN);
  isRaining = digitalRead(DIGITAL_PIN_RAIN) == LOW; // LOW bedeutet Regen
  String raining = isRaining ? "Yes" : "No";

  // Debugging-Ausgaben
  Serial.print("Analog Rain Value: ");
  Serial.println(rainVal);

  // Mapping the analog value to percentage
  rainVal = map(rainVal, 0, 4095, 100, 0); // Mapping to 0-100%
  rainVal = constrain(rainVal, 100, 0); // Ensure the value is within 0-100%

  Serial.print("Raining: ");
  Serial.println(raining);
  Serial.print("Moisture: ");
  Serial.print(rainVal);
  Serial.println("%");

  // Vibrationssensor
  vibrating = digitalRead(DIGITAL_PIN_VIBRATION);
  Serial.print("Vibration detected: ");
  Serial.println(vibrating);

  //Alarme////////////////////////////////////////////////////////////////
  //Erdbeben

  //Komet
  if (tType == "comet") {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = true;
    doc["severity"] = 3;
    doc["type"] = "Komet";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }
  else if (vibrating) {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = true;
    doc["severity"] = 3;
    doc["type"] = "Erdbeben";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }

  //Hitzewarnung
  else if (temperature > 35) {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = true;
    doc["severity"] = (temperature > 50) + (temperature > 40) + (temperature > 35);
    doc["type"] = "Hitzewarnung";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }

  //Kältewarnungen
  else if (temperature < -5) {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = true;
    doc["severity"] = (temperature < -10) + (temperature < -15) + (temperature < -25);
    doc["type"] = "Kältewarnung";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }

  //Hochwasser
  else if (tmoisture > 50) {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = true;
    doc["severity"] = (tmoisture > 75) + (tmoisture > 65) + (tmoisture > 50);
    doc["type"] = "Überschwemmung";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }


  //Entwarnung
  else {
    StaticJsonDocument<BUFFER_SIZE> doc;
    doc["alarm"] = false;
    doc["severity"] = 0;
    doc["type"] = "all-clear";
    publishJsonData(doc, "katastrophenschutz/alarm");
  }
  ////////////////////////////////////////////////////////////////////////////////


  delay(2000); // Delay before the next loop iteration
}
