// * Imports:
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// * Definitions:
#define ssid "MQTT-MINT"
#define password ""
#define mqtt_server "10.42.0.1"
#define MQTTPORT 1883

#define MAXSHOPS 30

#define flamePin 26
#define linePin 27
#define motionPin 25
#define green 2
#define yellow 0
#define red 4

// * Constants:
const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);

// * Global variables:
byte hour = 0;

bool catastrophyActive = false;
bool alarmActive = false;
bool irregularity = false;
bool einbruchAlarm = false;

int availableEnergy = 130;
int energyConsumption = 20;
int numCustomers = 0;
int shopsOpen = 0;

int lastBlink = 0;
int ledState = 0;

// * Setup of client-objects:
WiFiClient wifiClient;
PubSubClient client(wifiClient);

/** Conecting to WiFi */
void setup_wifi()
{
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect()
{
  // expl: Checking WiFi-Connection
  if ((WiFi.status() != WL_CONNECTED))
  {
    Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }
    Serial.println("SUCCESS");
  }
  // expl: Checking WiFi-Connection
  // expl: Retring
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("test/rieko", __TIME__);

      // ... and resubscribe
      client.subscribe("zeit");
      client.subscribe("feuerwache/abgearbeitet");
      client.subscribe("solarpark/energie/einkaufszentrum");
      client.subscribe("katastrophenschutz/alarm");

      /*for(int i; i < mqttSubNumber; i++){
        client.subscribe(mqttSubscriptions[i]);
      }*/
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

/** Serializing and sending JSON-Documents */
void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData, const char *topic)
{
  if (!client.connected())
  {
    reconnect();
  }

  size_t len = measureJson(jsonData) + 1;

  char buffer[len];
  size_t n = serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(topic, buffer, n);
}

bool isCrowded()
{
  return (hour == 12 || hour == 13 || hour == 19);
}

bool isNight()
{
  return (hour < 8 || hour > 19);
}

/* Calculate and publish Energy Consumption*/
void publishEnergyConsumption()
{
  if (shopsOpen == 0)
  {

    energyConsumption = 20;
  }
  else
  {
    energyConsumption = 100 + 10 * shopsOpen;
  }

  StaticJsonDocument<BUFFER_SIZE> doc;

  doc["value"] = energyConsumption;
  doc["unit"] = "kW";

  publishJsonData(doc, "einkaufszentrum/energie/verbrauch");
}

/* Calculate and publish Open Stores*/
void publishStoresOpen()
{
  String reason = "openingHours";

  if (catastrophyActive)
  {
    shopsOpen = 0;
    reason = "catastrophy";

    digitalWrite(yellow, LOW);
    digitalWrite(green, LOW);
  }
  else if (alarmActive)
  {
    shopsOpen = 0;
    reason = "alarm";

    digitalWrite(red, HIGH);
    digitalWrite(yellow, LOW);
    digitalWrite(green, LOW);
  }
  else if (isNight())
  {
    shopsOpen = 0;

    digitalWrite(red, LOW);
    digitalWrite(yellow, HIGH);
    digitalWrite(green, LOW);
  }
  else if (availableEnergy < 110)
  {
    shopsOpen = 0;
    reason = "energyShortage";

    digitalWrite(red, LOW);
    digitalWrite(yellow, HIGH);
    digitalWrite(green, HIGH);
  }
  else if (irregularity)
  {
    shopsOpen = 0;
    reason = "irregularity";

    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
  }
  else
  {
    shopsOpen = min(MAXSHOPS, (availableEnergy - 100) / 10);

    digitalWrite(red, LOW);
    digitalWrite(yellow, LOW);
    digitalWrite(green, HIGH);
  }

  StaticJsonDocument<BUFFER_SIZE> numDoc;

  numDoc["count"] = shopsOpen;
  numDoc["max"] = MAXSHOPS;

  publishJsonData(numDoc, "einkaufszentrum/geschaefte");

  StaticJsonDocument<BUFFER_SIZE> stateDoc;

  if (shopsOpen == 0)
  {
    stateDoc["state"] = "closed";
  }
  else
  {
    stateDoc["state"] = "open";
  }
  stateDoc["reason"] = reason;
  publishJsonData(stateDoc, "einkaufszentrum/offen");
}

void publishNumCustomer()
{
  int num = 0;
  for (int i = 0; i < shopsOpen; i++)
  {
    num = num + random(5l, 25);
  }

  if (isCrowded())
  {
    numCustomers = ((num * 3) / 2);
  }
  else
  {
    numCustomers = num;
  }

  StaticJsonDocument<BUFFER_SIZE> doc;

  doc["count"] = numCustomers;

  publishJsonData(doc, "einkaufszentrum/menschen");
}

void updateValues()
{
  publishStoresOpen();

  publishEnergyConsumption();

  publishNumCustomer();
}

void blinkLed(int pin, int wait)
{
  if ((millis() - lastBlink) > wait)
  {
    if (ledState == 0)
    {
      digitalWrite(pin, HIGH);
      ledState = 1;
    }
    else
    {
      digitalWrite(pin, LOW);
      ledState = 0;
    }
    lastBlink = millis();
  }
}

void checkFire()
{
  bool fire = !digitalRead(flamePin);

  if (fire && alarmActive == false)
  {

    alarmActive = true;

    StaticJsonDocument<BUFFER_SIZE> doc;

    doc["alarm"] = true;
    doc["origin"] = "einkaufszentrum";
    doc["type"] = "fire";

    publishJsonData(doc, "feuerwache/alarm");

    updateValues();
  }
}

void checkEinbrecher()
{
  if (digitalRead(motionPin) && isNight())
  {
    einbruchAlarm = true;

    StaticJsonDocument<BUFFER_SIZE> doc;

    doc["einbruch"] = true;
    doc["Zeit"] = hour;

    publishJsonData(doc, "einkaufszentrum/einbruch");
  }

  // expl: morgens wird Alarm zurückgesetzt
  else if (!isNight() && einbruchAlarm)
  {
    einbruchAlarm = false;

    StaticJsonDocument<BUFFER_SIZE> doc;

    doc["einbruch"] = false;

    publishJsonData(doc, "einkaufszentrum/einbruch");
  }
}

void checkIrregularity()
{
  if (!irregularity && digitalRead(linePin))
  {
    irregularity = true;

    updateValues();
    // Serial.println("line");
  }
  else if (irregularity && !digitalRead(linePin))
  {

    irregularity = false;
    updateValues();
  }
}

void callback(char *topic, byte *payload, unsigned int length)
{

  // Serial.print("Callback <");
  // Serial.print(topic);
  // Serial.print(">: ");
  // Serial.println((char *)payload);

  // Eingegeangene MQTT Nachricht als JSON bekommen
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  if (!jsonDoc["hour"].isNull())
  {
    int newhour = jsonDoc["hour"].as<int>();
    if (hour != newhour)
    {
      hour = newhour;
      updateValues();
    }
  }

  // expl: zugewiesene Energie
  if (!jsonDoc["value"].isNull())
  {
    int newEnergy = jsonDoc["value"].as<int>();
    if (availableEnergy != newEnergy)
    {
      availableEnergy = newEnergy;
      updateValues();
    }
  }

  // expl: Wenn feuerwache alarm abgearbeitet hat (feuerwache/abgearbeitet)
  if (!jsonDoc["origin"].isNull())
  {
    String origin = jsonDoc["origin"].as<String>();
    if (alarmActive == true && origin == "einkaufszentrum")
    {
      alarmActive = false;
      updateValues();
    }
  }

  // expl: Katastrophe vorhanden?
  if (!jsonDoc["severity"].isNull())
  {
    int severity = jsonDoc["severity"].as<int>();
    if (severity != 0)
    {
      catastrophyActive = true;
    }
    else
    {
      catastrophyActive = false;
    }
  }
}

void setup()
{

  pinMode(flamePin, INPUT);
  pinMode(linePin, INPUT);
  pinMode(motionPin, INPUT);

  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(red, OUTPUT);

  Serial.begin(115200);
  Serial.setTimeout(500); // Set time out
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, MQTTPORT);
  client.setCallback(callback);
  reconnect();
}

void loop()
{
  client.loop();

  checkFire();
  checkEinbrecher();
  checkIrregularity();

  if (irregularity)
  {
    blinkLed(yellow, 250);
  }
  if (catastrophyActive)
  {
    blinkLed(red, 250);
  }

  delay(500);
}
