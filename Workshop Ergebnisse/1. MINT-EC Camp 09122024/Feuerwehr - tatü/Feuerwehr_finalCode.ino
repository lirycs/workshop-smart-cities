//Feuerwehr
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>


//queue
#include <ArduinoQueue.h>

struct stAlarm {
  String aOrigin;
  String aType;
};
ArduinoQueue<stAlarm> qAlarm(10);


// Update these with values suitable for your network.
const char* ssid = "MQTT-MINT";
const char* mqtt_server = "10.42.0.1";
#define mqtt_port 1883
#define MQTT_SERIAL_PUBLISH_TOPIC "PUBLISH_TOPIC"
#define MQTT_SERIAL_RECEIVER_TOPIC "RECEIVE_TOPIC"

//line
#define DIGITAL_PIN 2
boolean line = false;
//

//Ampel
int GREEN = 15;
int YELLOW = 4;
int RED = 0;
//

int einsatzAnfang = 0;
bool existingDeployment = false;

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);
int control;

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      client.publish("/test", "hello world");
      // ... and resubscribe
      client.subscribe("zeit");
      client.subscribe("feuerwache/alarm");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte *payload, unsigned int length) {

  //Eingegeangene MQTT Nachricht als JSON bekommen
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  /*
    //Strings deserialisieren
    if (!jsonDoc["string"].isNull()) {
      String printable = jsonDoc["string"].as<String>();
      Serial.println(printable);
    }*/

  //alarme verarbeiten
  String topic_to_compare3 = "feuerwache/alarm";
  if (topic_to_compare3.equals(topic))
  {
    String aOrigin;
    String aType;
    if (!jsonDoc["origin"].isNull()) {
      aOrigin = jsonDoc["origin"].as<String>();
      Serial.println(aOrigin);
    }

    if (!jsonDoc["type"].isNull()) {
      aType = jsonDoc["type"].as<String>();
      Serial.println(aType);
    }
    alarmSpeichern(aOrigin, aType);
  }







  //Ints deserialisieren
  if (!jsonDoc["int"].isNull()) {
    int number = jsonDoc["int"].as<int>();
    Serial.println(number);
  }

  //Arrays deserialisieren
  JsonArray jsonArray = jsonDoc["array"];
  if (!jsonArray.isNull()) {
    for (JsonVariant v : jsonArray) {
      int number = v.as<int>();
      Serial.println(number);
    }
  }

  //Verschachtelte Objekte deserialisieren
  JsonObject jsonObj;
  jsonObj = jsonDoc.as<JsonObject>();
  jsonObj = jsonDoc["object"];

  /*
    Sobald eine Nachricht gesendet wird, leert sich das JsonDoc automatisch.
    Deshalb erst alles empfangen und dann senden!
  */

  //Leeres JSON Dokument erstellen
  const int capacity = JSON_OBJECT_SIZE(6);
  StaticJsonDocument<BUFFER_SIZE> doc;




  //Werte publishen

  //Energieverbrauch
  String topic_to_compare = "zeit";
  if (topic_to_compare.equals(topic))
  {
    doc["value"] = 50;
    doc["unit"] = "kw";
    publishJsonData(doc, "feuerwache/energie/verbrauch");
    //Serial.println("aktualisiert");
  }

  //auslösen
  String topic_to_compare2 = "feuerwache/alarm";
  if (topic_to_compare2.equals(topic)) {
    ampel();
  }

}

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(500);// Set time out
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();

  //Ampel
  pinMode(GREEN, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED, OUTPUT);
  digitalWrite(GREEN, HIGH);
  //
  //line
  pinMode(DIGITAL_PIN, INPUT);
  //
}

void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData, const char *topic) {
  if (!client.connected()) {
    reconnect();
  }


  size_t len = measureJson(jsonData) + 1;

  char buffer[len];
  size_t n = serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(topic, buffer, n); //brocker retain true
}

//////////////////////////////////////////////////////////////////////////////////////////
void ampel() {
  if (!existingDeployment) {
    line = digitalRead(DIGITAL_PIN);
    digitalWrite(RED, HIGH);
    digitalWrite(GREEN, LOW);
    Serial.println("Alarm, ");
    existingDeployment = true;

    //line
    if (line) {
      Serial.println("Line detected!"); // Reflektion wird als line erkannt; Licht = line detected

      digitalWrite(YELLOW, HIGH);
      einsatzAnfang = millis();
      Serial.println("Alarm bearbeiten, ");

    }

  }
}


//////////////////////////////////////////////////////////////////////////////////////////////

//Einsatz alarmSpeichern
void alarmSpeichern(String pOrigin, String pType) {
  stAlarm a {pOrigin, pType};
  qAlarm.enqueue(a);
}

//Einsatz abgearbeitet
void abgearbeitet() {

  //ampel
  digitalWrite(RED, LOW);
  digitalWrite(YELLOW, LOW);
  digitalWrite(GREEN, HIGH);

  Serial.println("Alarm fertig");
  //Leeres JSON Dokument erstellen
  const int capacity = JSON_OBJECT_SIZE(6);
  StaticJsonDocument<BUFFER_SIZE> doc;

  doc["alarm"] = false;
  if (!qAlarm.isEmpty()) {
    doc["origin"] = qAlarm.getHead().aOrigin;
    doc["type"] = qAlarm.getHead().aType;
  }
  publishJsonData(doc, "feuerwache/abgearbeitet");
  qAlarm.dequeue();
  existingDeployment = false;
}

void loop() {
  client.loop();
  if (!qAlarm.isEmpty() && millis() - einsatzAnfang >= 20000) {
    einsatzAnfang = 0;
    abgearbeitet();
  }

}
