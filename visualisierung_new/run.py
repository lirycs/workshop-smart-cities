from app import app, socketio
from app.models.mqtt_handeler import MQTTClient
from app.models.logger import Logger
from dotenv import load_dotenv
import os

load_dotenv()

mqttc = MQTTClient(os.getenv("MQTT_IP"))


if __name__ == '__main__':
    try:
        # try running Flask app with Websockets
        if mqttc.connect():
            mqttc.subscribe(["helloWorld/#"])
                
        Logger.setup_flask_logger(app=app, log_file='visualisierung_new/logs/app.log')
        app.logger.info("Starting the app....")
        socketio.run(app, host="0.0.0.0", port=5000, debug=True)
        
    except Exception as e:
        app.logger.error("Some unknown error occured.", str(e))
        raise SystemExit
    mqttc.disconnect()