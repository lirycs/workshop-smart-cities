"""Module that sets up logging"""
import logging
import os


class Logger:
    def setup_flask_logger(app, log_file, level=logging.INFO):
        """To setup as many loggers as you want"""
        try:
            if not os.path.exists('visualisierung_new/logs'):
                os.makedirs('visualisierung_new/logs')
            
            app.logger.setLevel(level)
            # Set Format of the logs 
            formatter = logging.Formatter('%(asctime)s | %(levelname)s \t| %(message)s')
            
            # Define logging to the consol
            console_handler = logging.StreamHandler()
            console_handler.setFormatter(formatter)
            
            # Define logging to a file
            file_handler = logging.FileHandler(log_file, mode='w')
            file_handler.setFormatter(formatter)
            
            # Add the loggers to the logger object
            app.logger.addHandler(file_handler)
            # app.logger.addHandler(console_handler)
            app.logger.info('Logger has been Setup')
        except Exception as e:
            print(f"An Exeption occured: {e}")
        
    
    def setup_mqtt_logger(log_file, levle=logging.INFO):
        """Function to setup a logger outside of the Flask-App context"""
        try:
            if not os.path.exists('visualisierung_new/logs'):
                os.makedirs('visualisierung_new/logs')
            # Create a logger object
            file_handler = logging.FileHandler(log_file)
            file_handler.setLevel(levle)
            
            # Create a console handler for logging to the console
            console_handler = logging.StreamHandler()
            console_handler.setLevel(levle)

            # Define the logging format for both handlers
            formatter = logging.Formatter(
                '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            )
            # file_handler.setFormatter(formatter)
            console_handler.setFormatter(formatter)
            file_handler.setFormatter(formatter)

            # Add both handlers to the logger
            logger = logging.getLogger('mqtt_handler')
            logger.setLevel(levle)
            logger.addHandler(file_handler)
            logger.addHandler(console_handler)
            return logger
        except Exception as e:
            print(f"An Exeption occured: {e}")
