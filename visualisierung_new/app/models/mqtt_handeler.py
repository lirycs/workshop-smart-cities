"""Modual for handeling logic forthe mqtt broker"""
import paho.mqtt.client as mqtt
from typing import List
import json
from app.models.logger import Logger
from app import socketio


class MQTTClient:
    """Class for connecting to a mqtt-broker/host

    Parameters:
    broker (str): The adress of the broker
    port (str)[optional]: The port of the broker

    Example usage:
        mqtt_client = MQTTClient()
        mqtt_client.connect()
        mqtt_client.subscribe("test/topic")

        # Get messages manually
        message = mqtt_client.get_message()

        # Or use callback
        mqtt_client.register_callback(lambda msg: print(f"New message: {msg}"))"""
    
    def __init__(self, broker: str = "localhost", port: int = 1883):
        self.broker = broker
        self.port = port
        self.client = mqtt.Client()
        
        # Configure logging
        self.logger = Logger.setup_mqtt_logger(log_file='visualisierung_new/logs/mqtt.log')
        
        # Set callbacks
        self.client.on_connect = self._on_connect
        self.client.on_message = self._on_message
        
    def _on_connect(self, client, userdata, flags, rc):
        self.logger.info(f"Connected to {self.broker} with result code {rc}")
        
    def _on_message(self, client, userdata, msg):
        try:
            message = json.loads(msg.payload.decode())
            self.logger.info(f"Received message on {msg.topic}: {message}")
            socketio.emit('mqtt_message', message)

            if msg.topic == "$SYS/broker/clients/total":
                socketio.emit("clients", {'data': msg.payload.decode(), 'topic': msg.topic})
                
        except json.JSONDecodeError:
            self.logger.error("Failed to decode message as JSON")
    
    def connect(self) -> bool:
        try:
            self.client.connect(self.broker, self.port, 60)
            self.client.loop_start()
            return True
        except Exception as e:
            self.logger.error(f"Connection failed: {e}")
            return False
            
    def disconnect(self):
        self.client.loop_stop()
        self.client.disconnect()
        
    def subscribe(self, topics: List):
        for topic in topics:
            self.client.subscribe(topic)
            self.logger.info(f"Subscribed to {topic}")
        
    def publish(self, topic: str, message: dict):
        try:
            self.client.publish(topic, json.dumps(message))
            self.logger.info(f"Published to {topic}: {message}")
        except Exception as e:
            self.logger.error(f"Failed to publish message: {e}")
