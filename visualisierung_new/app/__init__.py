from flask import Flask
from flask_socketio import SocketIO

app = Flask("MQTT-APP", template_folder='visualisierung_new/app/templates', static_folder='visualisierung_new/app/static')
socketio = SocketIO(app)

from app import routs
from app.logic import logic
from app.models import mqtt_handeler



