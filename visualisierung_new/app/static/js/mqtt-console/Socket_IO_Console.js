var socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on("connect", function () {
  hideNotConnectedBanner();
  console.log("Verbindung zum WebSocket-Server hergestellt.");
  connected();
});

socket.on("disconnect", function () {
  console.log("Verbindung zum WebSocket-Server unterbrochen.");
  showNotConnectedBanner();
  disconnected();
});

socket.on("reconnect", function (attemptNumber) {
  console.log("Verbindung zum WebSocket-Server wiederhergestellt. Versuch Nr.:", attemptNumber);
  alert("Die Verbindung zum Server wurde wiederhergestellt.");
  hideNotConnectedBanner();
  connected();
});

socket.on("reconnect_failed", function () {
  showNotConnectedBanner();
  console.log("Alle Wiederherstellungsversuche sind fehlgeschlagen.");
  disconnected();
});

socket.on('all_messages', function (msg) {
  if (stopBtnActive) {
    return;
  }
  let data = null;
  data = String(msg.data);
  let topic = String(msg.topic);

  createElement(topic, data);

});

function createElement(topic, dataStr) {
  if (isNullOrEmpty(dataStr)) {
    console.log("---");
    console.log("msg.data was null!");
    console.log("---");
    return;
  }

  var div = document.createElement("div");
  div.classList.add("col-12");
  div.classList.add("align-items-stretch");
  div.classList.add("mb-3");

  div.innerHTML = "<div class='card w-100'><div class='card-body'><h5 class='card-title'><b>Topic:</b> " + topic + "</h5><p class='card-text'> " + dataStr + "</p><p class='text-muted'>" + new Date() + "</p></div></div>";

  document.getElementById("namesContainer").appendChild(div);
  document.getElementById("namesContainer").insertBefore(div, document.getElementById("namesContainer").firstChild);
}